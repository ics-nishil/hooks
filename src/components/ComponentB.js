import React, { useContext } from 'react'
import ComponentC from './ComponentC'
import { UserContext } from '../App'

const ComponentB = () => {

  const user = useContext(UserContext)  
  return (
    <div>

        <h2>ComponentB :- Welcome, {user}</h2>    
        <ComponentC />

    </div>
  )
}

export default ComponentB