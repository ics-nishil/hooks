import React, { useEffect, useRef } from 'react'

const UseRef = () => {
    
    const inputRef = useRef(null)
    useEffect(()=>{
        inputRef.current.focus()
    },[])

    return (
    <div>
        <h1>UseRef</h1>
        <form>
            NAME : 
            <input ref={inputRef} type="text" />
            <br/>
            
            AGE : 
            <input type="number" />
            <br/>
            <button>Submit</button>
        </form>
        
        <hr/>
    </div>
    
  )
}

export default UseRef