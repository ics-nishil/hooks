import React from 'react'
import { useReducer } from "react";

const UseReducer = () => {


    const initialTodos = [
        {
          id: 1,
          title: "BUY MILK",
          complete: true,
        },
        {
          id: 2,
          title: "BUY BREAD",
          complete: false,
        },
        {
            id: 3,
            title: "BUY BUTTER",
            complete: false,
        },
      ];
      
      const reducer = (state, action) => {
        switch (action.type) {
          case "COMPLETE":
            return state.map((todo) => {
              if (todo.id === action.id) {
                return { ...todo, complete: !todo.complete };
              } else {
                return todo;
              }
            });
          default:
            return state;
        }
      };
      
    const [todos, dispatch] = useReducer(reducer, initialTodos);
    
    const handleComplete = (todo) => {
        dispatch({ type: "COMPLETE", id: todo.id });
    };
      




  return (
    <div>
        <hr/>
        <h1>
            UseReducer    
        </h1>

        {todos.map((todo) => (
        <div key={todo.id}>
          <label>
            <input
              type="checkbox"
              checked={todo.complete}
              onChange={() => handleComplete(todo)}
            />
            {todo.title}
          </label>
        </div>
      ))}

        <hr/>

    </div>
  )
}

export default UseReducer