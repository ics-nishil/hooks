import React , {memo} from 'react'

const UseCallback = ({ todos, addTodo }) => {
  console.log("child render");
  return (
    <div>
        <h1>
            UseCallback
        </h1>
        
        <h2>My Todo's</h2>
        
        {todos.map((todo, index) => 
           <p key={index}>{todo}</p>
        )}

        <button onClick={addTodo}>Add ToDo</button>
            

    </div>
  )
}

export default memo(UseCallback)