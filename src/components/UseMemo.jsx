import React, { useMemo, useState } from 'react'

const UseMemo = () => {
  
    const [countOne, setCountOne] = useState(0);
    const [countTwo, setCountTwo] = useState(0);

    const incrementOne = () => {
        setCountOne(pre=> pre + 1)
    }
    
    const incrementTwo = () => {
        setCountTwo(pre=>pre+1)
    }

    const isEven = useMemo(() => {
        let i=0;
        while(i<2000000000){
            i++
        }
        return countOne % 2 === 0
    },[countOne])
    
    return (
    <div>
        <hr/>
        <h1>UseMemo</h1>
        {/* used to cache the result of invoked function */}
    
        <button onClick={incrementOne}>Count One - {countOne}</button>
        <span> {isEven ? "even":"odd"} </span>
        <br/>
        <button onClick={incrementTwo}>Count Two - {countTwo}</button>

        <hr/>

    </div>
  )
}

export default UseMemo