import React from 'react'
import ComponentB from './ComponentB'

const ComponentA = () => {
  return (
    <div>
        <hr/>
        <h1>UseContext</h1>
    
        <ComponentB />

    </div>
  )
}

export default ComponentA