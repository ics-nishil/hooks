import React from 'react'
import { UserContext } from '../App'


const ComponentC = () => {
  return (
    <div>
        <UserContext.Consumer>
            {
                user =>{
                    return <h2>ComponentC :- WELCOME, {user}</h2>
                }
            }
        </UserContext.Consumer>
    </div>
  )
}

export default ComponentC