import './App.css';
import React, { useState, useCallback } from 'react';
import ComponentA from './components/ComponentA';
import UseMemo from './components/UseMemo';
import UseRef from './components/UseRef';
import UseCallback from './components/UseCallback';
import UseReducer from './components/UseReducer';

export const UserContext = React.createContext()

function App() {

  const [todos, setTodos] = useState([]);
  const [count, setCount] = useState(0);
  
  const increment = () => {
    setCount((c) => c + 1);
  };

  const addTodo = useCallback(() => {
    setTodos((t) => [...t, "New Todo"]);
  }, [todos]);


  return (
    <div className="App">
      <UserContext.Provider value={'NISHIL'}>
        <ComponentA />
      </UserContext.Provider>

      <UseMemo/>
    
      <UseRef/>
    
      <UseCallback todos={todos} addTodo={addTodo} />
      <div>
        Count: {count}
        <button onClick={increment}>+</button>
      </div>
    
      <UseReducer/>
    </div>
  );
}

export default App;
